﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feonufry.CUI.Actions;

namespace Command
{
    public class Run:IAction
    {
        static void Main(string[] args)
        {
        }

        public void Perform(ActionExecutionContext context)
        {
            // Создаем пользователя.
            User user = new User();

            // Пусть он что-нибудь сделает.
            user.Compute('+', 100);
            user.Compute('-', 50);
            user.Compute('*', 10);
            user.Compute('/', 2);

            // Отменяем 4 команды
            user.Undo(4);

            // Вернём 3 отменённые команды.
            user.Redo(3);
        }
    }
}
