﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feonufry.CUI.Actions;

namespace Strategy
{
    public class Run:IAction
    {
        static void Main(string[] args)
        {
        }

        public void Perform(ActionExecutionContext context)
        {
            // Создаём контекст и инициализируем его первой стратегией.
            Context cOntext = new Context(new ConcreteStrategy1());
            // Выполняем операцию контекста, которая использует первую стратегию.
            cOntext.ExecuteOperation();
            // Заменяем в контексте первую стратегию второй.
            cOntext.SetStrategy(new ConcreteStrategy2());
            // Выполняем операцию контекста, которая теперь использует вторую стратегию.
            cOntext.ExecuteOperation();
        }
    }
}
