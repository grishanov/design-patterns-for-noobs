﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    class Context
    {
        // Ссылка на интерфейс IStrategy
        // позволяет автоматически переключаться между конкретными реализациями
        // (другими словами, это выбор конкретной стратегии).
        private IStrategy _strategy;

        // Конструктор контекста.
        // Инициализирует объект стратегией.
        public Context(IStrategy strategy)
        {
            _strategy = strategy;
        }

        // Метод для установки стратегии.
        // Служит для смены стратегии во время выполнения.
        // В C# может быть реализован также как свойство записи.
        public void SetStrategy(IStrategy strategy)
        {
            _strategy = strategy;
        }

        // Некоторая функциональность контекста, которая выбирает
        //стратегию и использует её для решения своей задачи.
        public void ExecuteOperation()
        {
            _strategy.Algorithm();
        }
    }
}
