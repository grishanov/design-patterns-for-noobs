﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feonufry.CUI.Actions;

namespace Prototype
{
    public class Run:IAction
    {
        static void Main(string[] args)
        {
        }

        public void Perform(ActionExecutionContext context)
        {
            // Create two instances and clone each 
            Prototype prototype1 = new ConcretePrototype1("I");
            Prototype clonedPrototype1 = prototype1.Clone();
            Console.WriteLine("Cloned: {0}", clonedPrototype1.Id);

            Prototype prototype2 = new ConcretePrototype2("II");
            Prototype clonedPrototype2 = prototype2.Clone();
            Console.WriteLine("Cloned: {0}", clonedPrototype2.Id);

        }
    }
}
