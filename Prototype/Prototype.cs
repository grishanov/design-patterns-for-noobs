﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
    abstract class Prototype
    {
        public Prototype(string id)
        {
            this.Id = id;

            Console.Write("Base constructor is called.");
        }

        // Property
        public string Id { get; private set; }

        public virtual Prototype Clone()
        {
            // Shallow copy
            return (Prototype)this.MemberwiseClone();
        }
    }
}
