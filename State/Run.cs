﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feonufry.CUI.Actions;

namespace State
{
    public class Run:IAction
    {
        static void Main(string[] args)
        {
        }

        public void Perform(ActionExecutionContext context)
        {
            var automat = new Automat(9);

            automat.GotApplication();
            automat.CheckApplication();
            automat.RentApartment();
        }
    }
}
