﻿using System.ComponentModel.Design;
using System.Data;
using System.Runtime.Remoting.Proxies;
using Feonufry.CUI.Menu.Builders;
using Strategy;

namespace DesignPatterns
{
    class MainApp
    {
        static void Main(string[] args)
        {
            new MenuBuilder()
                .Title("Design Patterns")
                .Repeatable()
                .Submenu()
                      .Title("Creational Patterns")
                      .Item("Abstract factory", new AbstractFactory.Run())
                      .Item("Prototype", new Prototype.Run())
                      .Item("Factory Method", new FactoryMethod.Run())
                      .Item("Builder", new Builder.Run())
                      .Exit("Назад")
                .End()
                .Submenu()
                      .Title("Structural Patterns")
                      .Item("Composite", new Composite.Run())
                      .Item("Adapter", new Adapter.Run())
                      .Item("Decorator", new Decorator.Run())
                      .Item("Facade", new Facade.Run())
                      .Item("Proxy", new Proxy.Run())
                      .Exit("Назад")
                .End()
                .Submenu()
                      .Title("Behavioral Patterns")
                      .Item("Observer", new Observer.Run())
                      .Item("Iterator",new Iterator.Run())
                      .Item("Command", new Command.Run())
                      .Item("Mediator", new Mediator.Run())
                      .Item("State", new State.Run())
                      .Item("Strategy", new Strategy.Run())
                      .Item("Template method", new TemplateMethod.Run())
                      .Item("Visitor",new Visitor.Run())
                      .Exit("Назад")
                .End()
                .Exit("Exit program")
                .GetMenu().Run();
        }
    }
}
