﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateMethod
{
    class Monopoly:GameObject
    {
        /* Implementation of necessary concrete methods */

        protected override void InitializeGame()
        {
            // Initialize money
        }

        protected override void MakePlay(int player)
        {
            // Process one turn of player
        }

        protected override bool EndOfGame()
        {
            return true;
        }

        protected override void PrintWinner()
        {
            // Display who won
            Console.WriteLine("Display who won");
        }

        /* Specific declarations for the Monopoly game. */

        // ...
    }
}
