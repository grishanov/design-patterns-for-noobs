﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateMethod
{
    class Chess:GameObject
    {
        /* Implementation of necessary concrete methods */

        protected override void InitializeGame()
        {
            // Put the pieces on the board
        }

        protected override void MakePlay(int player)
        {
            // Process a turn for the player
        }

        protected override bool EndOfGame()
        {
            return true;
            // Return true if in Checkmate or Stalemate has been reached
        }

        protected override void PrintWinner()
        {
            // Display the winning player
        }

        /* Specific declarations for the chess game. */

        // ...

    }
}
