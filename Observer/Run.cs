﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feonufry.CUI.Actions;

namespace Observer
{
    public class Run:IAction
    {
        static void Main(string[] args)
        {
        }

        public void Perform(ActionExecutionContext context)
        {
            Subject subject = new Subject();
            Observer observer = new Observer(subject, "Center", "\t\t");
            Observer observer2 = new Observer(subject, "Right", "\t\t\t\t");
            subject.Go();
        }
    }
}
