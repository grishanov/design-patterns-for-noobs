﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy
{
    class MathProxy:IMath
    {
        Math math;

        public MathProxy()
        {
            math = null;
        }

        /// <summary>
        /// Быстрая операция - не требует реального субъекта
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public double Add(double x, double y)
        {
            return x + y;
        }

        public double Sub(double x, double y)
        {
            return x - y;
        }

        /// <summary>
        /// Медленная операция - требует создания реального субъекта
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public double Mul(double x, double y)
        {
            if (math == null)
                math = new Math();
            return math.Mul(x, y);
        }

        public double Div(double x, double y)
        {
            if (math == null)
                math = new Math();
            return math.Div(x, y);
        }
    }
}
