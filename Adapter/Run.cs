﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Feonufry.CUI.Actions;

namespace Adapter
{
    public class Run:IAction
    {
        static void Main(string[] args)
        {
        }

        public void Perform(ActionExecutionContext context)
        {
            // Create adapter and place a request
            Target target = new Adapter();
            target.Request();

            Application.EnableVisualStyles();
            Application.Run(new Info());
        }
    }
}
